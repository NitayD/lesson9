<?php
	echo "<center><h1>Вопрос 1: Распишите своё понимание инкапсуляции.</h1></center>";
	echo "<h2 class=\"answer\">Инкапсуляция - это сбор всей логики, методов, свойств, инструкций и др. в один шаблон, в один класс, который скрыт от глаз пользователя.</h2><hr>";
	echo "<center><h1>Вопрос 2: Сформулируйте своими словами в чём плюсы объектов, а в чём минусы?</h1></center>";
	echo "<h2 class=\"answer\">Плюсы:<br>1. Всегда по шаблону. Не нужно писать много лишних строк кода - их можно указать в конструкторе.<br>2. Каждый обьект имеет стандартные методы (если они есть в классе).<br>3. Отпадает необходимость бегать по коду, в поисках нужных свойств и фуекций - всё находится в классе.<br>4. Защита кода от 3-тих лиц.</h2><br>";
	echo "<h2 class=\"answer\">Минусы:<br>1. Нету минусов, это всегда удобно.<br>2. Так  как существуют \"магические\" методы (например: конструктор), все возможные минусы уже не существуют.</h2>";
/////////////////////////////////////////////////////////////////////////////////////////
	class House
	{
		private $floors;
		private $apartments;
		private $wall_color = 'white';
		public function __construct($floors, $apartaments, $wall_color, $width, $length)
		{
			$this->floors = $floors;
			$this->apartaments = $apartaments;
			$this->wall_color = $wall_color;
			$this->width = $width . 'м';
			$this->height = $height . 'м';
		}
		public function outputFloors()
		{
			$outputData = 'В этом доме ' . $this->floors . ' этажа.' . "<br>";
			return $outputData;
		}
		public function outputApartaments()
		{
			$outputData = 'В этом доме ' . $this->apartaments . ' квартиры.' . "<br>";
			return $outputData;
		}
		public function outputWall_color()
		{
			$outputData = 'В этом доме ' . $this->wall_color . ' цвет стен.' . "<br>";
			return $outputData;
		}
		public function outputWidth()
		{
			$outputData = 'У этого дома ' . $this->width . ' ширина.' . "<br>";
			return $outputData;
		}
		public function outputLength()
		{
			$outputData = 'У этого дома ' . $this->length . ' длинна.' . "<br>";
			return $outputData;
		}
	}
	$my_house = new House(4, 100, 'beige', 100, 40);
	//First object///////////////////////////
	class Real_object//Шкаф
	{
		private $width;
		private $height;
		private $weight;
		private $length;
		private $age;
		function __construct($width, $height, $weight, $length, $age)
		{
			$this->width = $width . 'см';
			$this->height = $height . 'см';
			$this->weight = $weight . 'кг';
			$this->length = $length . 'см';
			$this->age = $age . ' лет';
		}
		public function outputWidth()
		{
			$outputData = $this->width . "<br>";
			return $outputData;
		}
		public function outputHeight()
		{
			$outputData = $this->height . "<br>";
			return $outputData;
		}
		public function outputWeight()
		{
			$outputData = $this->weight . "<br>";
			return $outputData;
		}
		public function outputLength()
		{
			$outputData = $this->length . "<br>";
			return $outputData;
		}
		public function outputAge()
		{
			$outputData = $this->age . "<br>";
			return $outputData;
		}
	}
	$car = new Real_object(100, 70, 2500, 230, 15);
	$roadCity = new Real_object(300, 30, NULL, NULL, 10);
	$curtain = new Real_object(400, 300, 2, 0.1, 1);
	$systemBlock = new Real_object(10, 40, 3, 50, 3);
